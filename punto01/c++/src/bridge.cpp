#include "bridge.h"
#include <iostream>

using namespace std;

static void printCar(const char* place, const int nCar) {
  cout << "nCar: " << nCar << place << endl;
}

Bridge::Bridge() : nCarsOnBridge(0) { }
Bridge::~Bridge() { }

void Bridge::enterWest(int nCar) {
  printCar(" is entering by west", nCar);
  // This must be increased after has been effectively entered
  nCarsOnBridge++;
}

void Bridge::enterEast(int nCar) {
  printCar(" is entering by east", nCar);
  // This must be increased after has been effectively entered
  nCarsOnBridge++;
}

void Bridge::leaveWest(int nCar) {
  printCar(" is leaving by west", nCar);
  // This must be reduced after has been effectively leaving
  nCarsOnBridge--;
}

void Bridge::leaveEast(int nCar) {
  printCar(" is leaving by east", nCar);
  // This must be reduced after has been effectively leaving
  nCarsOnBridge--;
}

int Bridge::getNCarsOnBridge() const {
  return nCarsOnBridge;
}
