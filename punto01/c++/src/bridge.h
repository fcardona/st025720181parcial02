#pragma once

class Bridge {
  int nCarsOnBridge;
 public:
  Bridge();
  ~Bridge();

  int  getNCarsOnBridge() const;
  void enterWest(int nCar);
  void enterEast(int nCar);
  void leaveWest(int nCar);
  void leaveEast(int nCar);
};
